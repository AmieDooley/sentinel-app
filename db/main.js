const db = require('./index');

const admin = {
    async getAllStudents() {
        return new Promise(async (resolve, reject) => {
          db.query(`SELECT * from sentinel.Student`, (err, results) => {
                  if(err){
                    console.log(err);
                    reject(new Error("error reading from db"))
                  }
                  resolve(results)
                })
        })
    },
    async getAllStudentModuleGrades() {
        return new Promise(async (resolve, reject) => {
            db.query(`
                SELECT s.name, s.employeeID, m.courseCode, m.shortName, m.moduleId, g.gradeId, g.grade, g.module 
                from sentinel.Student AS s 
                    left JOIN sentinel.Module AS m 
                        ON m.courseCode = s.courseCode 
                    RIGHT join sentinel.Grade AS g 
                        ON m.moduleId = g.module AND s.employeeID = g.student
            `, (err, results) => {
                if(err){
                    console.log(err);
                    reject(new Error("error reading from db"))
                }
                resolve(results)
            })
        })
    },

};

module.exports = admin 