import * as React from "react";
import Rechart2 from "../Components/Rechart2";
import mediterranean from "../Colours/Mediterranean";
import reds from "../Colours/Reds";
import greens from "../Colours/Greens";
import greys from "../Colours/Greys";
import purples from "../Colours/Purples";
import blues from "../Colours/Blues";
import colourBlind from "../Colours/ColourBlind";
import pastels from "../Colours/Pastels";
import Filter from "../Components/Filter";

let StudentResults = () => {
    const [colourScheme, setColourScheme] = React.useState(mediterranean)
    const [studentNames, setStudentNames] = React.useState([])
    const [dataToRender, setDataToRender] = React.useState([])
    const [studentModules, setStudentModules] = React.useState([])

    React.useEffect(() => {
        (async () => {
            const response = await fetch('/api/studentsModules', {
                method: 'GET',
            })
            let jsonData= await response.text()
            let formattedData = formatStudentModule(jsonData)
            setStudentModules(formattedData)
            let studentNamesArr = []
            studentModules.map((student, i) => (
                studentNamesArr[i] = {value: student.name, label: student.name}
            ))
            studentNamesArr.push({value: "All", label: "All"})
            setStudentNames(studentNamesArr)
            setDataToRender(studentModules)
        })();
    }, []);

    const formatStudentModule = (jsonString) => {
        let currentEmployee = ""
        let index
        let studentDetails = []
        let json = JSON.parse(jsonString)
        for (let key in json){
            let row = json[key];
            if (row["employeeID"] !== currentEmployee){
                index == undefined ? index = 0 : index++
                currentEmployee = row["employeeID"];
                studentDetails[index] = {employeeID: currentEmployee, name: row["name"]}
            }
            let module = row["shortName"];
            let grade = row["grade"]
            studentDetails[index][module]= grade;
        }
        return studentDetails
    }


    const tables = () => {
        const bars = dataToRender.map((student, i) => {
            const keys = [];
            for(let key in student){
                if (!(key === "name" || key === "employeeID")){
                    keys.push(key)
                }
            }
            return (
                <div className={`chart-wrp`}>
                    <h3>{student.name}'s results</h3>
                    <div className={`bar-wrp`}>
                        <Rechart2 student={[student]} colourScheme={colourScheme} keys={keys}/>
                    </div>
                </div>
            );
        });

        return bars
    }

    const changeColour = (value) => {
        setColourScheme(value)
    }

    const colourOptions = [
        {value: colourBlind, label: "ColourBlind Friendly"},
        {value: reds, label: "Reds"},
        {value: blues, label: "Blues"},
        {value: greens, label: "Greens"},
        {value: purples, label: "Purples"},
        {value: greys, label: "Greys"},
        {value: pastels, label: "Pastels"},
        {value: mediterranean, label: "Mediterranean"},
    ]

    const filterStudent = (value) => {
        if(value === "All"){
            setDataToRender(studentModules)
        } else {
            let student =[]
            for(let i = 0; i<studentModules.length; i++){
                if(studentModules[i].name === value){
                    student = [studentModules[i]]
                }
            }
            setDataToRender(student)
        }
    }

        return (
            <div className={`container wide`}>
                <div className={`row h-centre title`}>
                    <div className={`col d-full`}>
                        <div className={`centred`}>
                            <h2>Individual Student Grades </h2>
                        </div>
                    </div>
                </div>
                <div className={`row filter-wrp`}>
                    <Filter title={'ColourScheme'} options={colourOptions} onChangeFunc={changeColour} />
                    <Filter title={'Student'} options={studentNames} onChangeFunc={filterStudent} />
                </div>
                <div className={`row`}>
                    <div className={`col d-full viz-wrp centred`}>
                        <div className={`row`}>
                                {tables()}
                        </div>
                    </div>
                </div>
            </div>
        );
}

export default StudentResults;