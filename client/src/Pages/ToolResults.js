import * as React from "react";
import Rechart from "../Components/Rechart";
import mediterranean from "../Colours/Mediterranean";
import reds from "../Colours/Reds";
import greens from "../Colours/Greens";
import greys from "../Colours/Greys";
import purples from "../Colours/Purples";
import blues from "../Colours/Blues";
import colourBlind from "../Colours/ColourBlind";
import pastels from "../Colours/Pastels";
import studentGradesPerTool from "../Data/StudentGradesPerTool";
import Filter from "../Components/Filter";

let ToolResults = () => {
    const [colourScheme, setColourScheme] = React.useState(mediterranean)
    const [dataToRender, setDataToRender] = React.useState([])
    const [toolOptions, setToolOptions] = React.useState([])
    const [allTools, setAllTools] = React.useState([])

    React.useEffect(() => {
        const keys = [];
        const toolOpts = []
        const student = studentGradesPerTool[0]
        for(let key in student){
            if (!(key === "name" || key === "employeeID")){
                keys.push(key)
                toolOpts.push({value: key, label: key })
            }
        }
        toolOpts.push({value: "All Tools", label: "All Tools"})
        setDataToRender(keys)
        setToolOptions(toolOpts)
        setAllTools(keys)
    }, []);

    const changeColour = (value) => {
        setColourScheme(value)
    }

    const filerTool = (value) => {
        if(value === "All Tools"){
            setDataToRender(allTools)
        } else {
            setDataToRender([value])
        }
    }

    const colourOptions = [
        {value: colourBlind, label: "ColourBlind Friendly"},
        {value: reds, label: "Reds"},
        {value: blues, label: "Blues"},
        {value: greens, label: "Greens"},
        {value: purples, label: "Purples"},
        {value: greys, label: "Greys"},
        {value: pastels, label: "Pastels"},
        {value: mediterranean, label: "Mediterranean"},
    ]

        return (
            <div className={`container wide`}>
                <div className={`row h-centre title`}>
                    <div className={`col d-full`}>
                        <div className={`centred`}>
                            <h2>Average Tool Grades</h2>
                        </div>
                    </div>
                </div>
                <div className={`row`}>
                    <Filter title={'ColourScheme'} options={colourOptions} onChangeFunc={changeColour} />
                    <Filter title={'Tool/Technology'} options={toolOptions} onChangeFunc={filerTool}/>
                </div>
                <div className={`row`}>
                    <div className={`col d-full viz-wrp`}>
                        <div className={`row`}>
                            <div className={`col`}>
                                <Rechart colourScheme={colourScheme} data={studentGradesPerTool} keys={dataToRender}/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
}

export default ToolResults;


