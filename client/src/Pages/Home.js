import React from "react";
import VisTabs from "../Components/VisTabs";


let  Home = () => {
        return (
            <div className={`container`}>
                <div className={`row h-centre title`}>
                    <h3>Categories</h3>
                </div>
                <VisTabs />
            </div>
        );

}

export default  Home;