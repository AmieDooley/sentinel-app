import * as React from "react";
import mediterranean from "../Colours/Mediterranean";
import reds from "../Colours/Reds";
import greens from "../Colours/Greens";
import greys from "../Colours/Greys";
import purples from "../Colours/Purples";
import blues from "../Colours/Blues";
import colourBlind from "../Colours/ColourBlind";
import pastels from "../Colours/Pastels";
import studentGradesPerTool from "../Data/StudentGradesPerTool";
import PieChart from "../Components/PieChart";
import boundaries from "../Data/boundaries";
import Filter from "../Components/Filter";

let ModuleResults = () => {
    const [colourScheme, setColourScheme] = React.useState(mediterranean)
    const [performanceData, setPerformanceData] = React.useState([])

    React.useEffect(() => {
        let splitModuleGrades = {};
        const student = studentGradesPerTool[0]
        for (let key in student) {
            if (!(key === "name" || key === "employeeID")) {
                splitModuleGrades[key] = [];
            }
        }
        studentGradesPerTool.forEach((student, index) => {
            for(let topic in student){
                if (!(topic === "name" || topic === "employeeID")) {
                    splitModuleGrades[topic].push(student[topic])
                }
            }
        })
        setPerformanceData(splitModuleGrades)
    }, []);

    const changeColour = (value) => {
        setColourScheme(value)
    }
    const colourOptions = [
        {value: colourBlind, label: "ColourBlind Friendly"},
        {value: reds, label: "Reds"},
        {value: blues, label: "Blues"},
        {value: greens, label: "Greens"},
        {value: purples, label: "Purples"},
        {value: greys, label: "Greys"},
        {value: pastels, label: "Pastels"},
        {value: mediterranean, label: "Mediterranean"},
    ]

    const boundaryCalc = (value) => {
        for(let i = 0; i < boundaries.length; i++){
            if(value >= boundaries[i].valueLower && value <= boundaries[i].valueUpper){
                return boundaries[i].label
            }
        }
        return `Error: Grade ${value} does not match any known boundaries`;
    }

    const getBoundaryData = () => {
        let boundaryData = {};
        for(let topic in performanceData){
            const boundaryCount = {'First Class (1st)': 0, 'Upper Second Class (2:1)': 0,
                'Lower Second Class (2:2)': 0, 'Third Class (3rd)': 0}
            performanceData[topic].forEach((result) => {
                try{
                    let boundary = boundaryCalc(result)
                    boundaryCount[boundary]++
                } catch (e) {
                    console.error(e)
                }
            })
            boundaryData[topic] = boundaryCount;
        }
        let arr = [];
        for(let topic in boundaryData){
            let topicData = {label: topic, data: []}
            for(let boundary in boundaryData[topic]){
                let value = boundaryData[topic][boundary]
                let name = boundary
                topicData.data.push({name: name, value: value})
            }
            arr.push(topicData)
        }
        return arr;
    }

    const PieCharts = () => {
        let data = getBoundaryData()
        const pies = data.map((topic, i) => {
            return (
                <div className={`pie-wrp`} key={data[i].label}>
                    <h3>{data[i].label}</h3>
                    <PieChart colourScheme={colourScheme} data={data[i].data}/>
                </div>
            )
        });
        return pies
    }

        return (
            <div className={`container wide`}>
                <div className={`row h-centre title`}>
                    <div className={`col d-full`}>
                        <div className={`centred`}>
                            <h2>Performance per tool</h2>
                        </div>
                    </div>
                </div>
                <div className={`row filter-wrp`}>
                    <Filter title={'ColourScheme'} options={colourOptions} onChangeFunc={changeColour} />
                </div>
                <div className={`row`}>
                    <div className={`col d-full viz-wrp centred`}>
                                {PieCharts()}
                    </div>
                </div>
            </div>
        );
}

export default ModuleResults
