import * as React from "react";
import Rechart from "../Components/Rechart";
import Filter from "../Components/Filter";
import mediterranean from "../Colours/Mediterranean";
import reds from "../Colours/Reds";
import greens from "../Colours/Greens";
import greys from "../Colours/Greys";
import purples from "../Colours/Purples";
import blues from "../Colours/Blues";
import colourBlind from "../Colours/ColourBlind";
import pastels from "../Colours/Pastels";
import studentGradesPerModule from "../Data/StudentGradesPerModule";

let ModuleResults = () => {
    const [colourScheme, setColourScheme] = React.useState(mediterranean)
    const [dataToRender, setDataToRender] = React.useState([])
    const [moduleOptions, setModuleOptions] = React.useState([])
    const [allModules, setAllModules] = React.useState([])

    React.useEffect(() => {
        const keys = [];
        const modOptions = []
        const student = studentGradesPerModule[0]
        for(let key in student){
            if (!(key === "name" || key === "employeeID")){
                keys.push(key)
                modOptions.push({value: key, label: key})
            }
        }
        modOptions.push({value: "All", label: "All"})
        setDataToRender(keys)
        setModuleOptions(modOptions)
        setAllModules(keys)
    }, []);


    const changeColour = (value) => {
        setColourScheme(value)
    }

    const filerModule = (value) => {
        if(value === "All"){
            setDataToRender(allModules)
        } else {
            setDataToRender([value])
        }
    }

    const colourOptions = [
        {value: colourBlind, label: "ColourBlind Friendly"},
        {value: reds, label: "Reds"},
        {value: blues, label: "Blues"},
        {value: greens, label: "Greens"},
        {value: purples, label: "Purples"},
        {value: greys, label: "Greys"},
        {value: pastels, label: "Pastels"},
        {value: mediterranean, label: "Mediterranean"},
    ]
        return (
            <div className={`container wide`}>
                <div className={`row h-centre title`}>
                    <div className={`col d-full`}>
                        <div className={`centred`}>
                            <h2>Grades per module </h2>
                        </div>
                    </div>
                </div>
                <div className={`row filter-wrp`}>
                    <Filter title={'ColourScheme'} options={colourOptions} onChangeFunc={changeColour} />
                    <Filter title={'Module'} options={moduleOptions} onChangeFunc={filerModule}/>
                </div>
                <div className={`row`}>
                    <div className={`col d-full viz-wrp`}>
                        <div className={`row`}>
                            <div className={`col`}>
                                <Rechart colourScheme={colourScheme} data={studentGradesPerModule} keys={dataToRender}/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
}

export default ModuleResults
