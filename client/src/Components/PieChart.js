import React from 'react';
import { PieChart, Pie, Cell, Legend } from 'recharts';


const RADIAN = Math.PI / 180;
const renderCustomizedLabel = ({cx, cy, midAngle, innerRadius, outerRadius, percent}) => {
    const radius = innerRadius + (outerRadius - innerRadius) * 0.5;
    const x = cx + radius * Math.cos(-midAngle * RADIAN);
    const y = cy + radius * Math.sin(-midAngle * RADIAN);

    return (
        <text x={x} y={y} fill="white" textAnchor="middle" dominantBaseline="central">
            {`${(percent * 100).toFixed(0)}%`}
        </text>
    );
}

const PieChartInfo = ({colourScheme, data}) => {
    const [studentNo, setStudentNo] = React.useState(0)

    React.useEffect(() => {
        (() => {
            let num = 0
            data.map((boundary) => (
                num += boundary.value
            ))
            setStudentNo(num);

        })();
    }, [data]);


        return (
                <PieChart width={600} height={600}>
                    <Pie
                        data={data}
                        cx="50%"
                        cy="50%"
                        labelLine={false}
                        isAnimationActive={false}
                        label={renderCustomizedLabel}
                        outerRadius={200}
                        fill="#8884d8"
                        dataKey="value"
                    >
                        {data.map((entry, index) => (
                            <Cell key={`cell-${index}`} fill={colourScheme[index % colourScheme.length]} />
                        ))}
                    </Pie>
                    <Legend
                        payload={
                            data.map(
                                (item, index) => ({
                                    id: item.name,
                                    type: "square",
                                    value: `${item.name} (${item.value} / ${studentNo} students)`,
                                    color: colourScheme[index % colourScheme.length]
                                })
                            )
                        }
                    />
                </PieChart>
        );
}

export default PieChartInfo;
