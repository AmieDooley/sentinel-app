import * as React from "react";

const Button = ({name, description}) => {
    const [slug, setSlug] = React.useState('')

    React.useEffect(() => {
        let hyphenSlug = name.replace(/\s+/g, '-').toLowerCase();
        setSlug(hyphenSlug)
    }, [name]);


    return (
        <div className={`button-wrp align-centre`}>
            <a href={`/viz/${slug}`}>
                <button className={`lg-button`}>{name}
                    <p>{description}</p>
                </button>
            </a>
        </div>
    )
};
export default Button;
