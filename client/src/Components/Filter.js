import * as React from "react";
import Select from "react-select";

const Filter = ({title, options, onChangeFunc}) => {
    return (
        <div className={`col d-third filter`}>
            <div className={'filterWrapper'}>
                <p>{title}:</p>
                <Select name='Select' classNamePrefix='filter' className="filterSelect" options={options} onChange={event => onChangeFunc(event.value)}/>
            </div>
        </div>
    )
};
export default Filter;

