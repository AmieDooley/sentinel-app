import logo from "../img/rsLogo.png";
import * as React from "react";


const Tab = ({title, slug}) => {
    return (
        <div className={`col d-quarter align-centre menu-tab`}>
            <a href={`/${slug}`}>
                <p>{title}</p>
            </a>
        </div>
    )
}

const Header = () => {
    return (
        <header className={`App-header`}>
            <div className={`container full-width`}>
                <div className={`row h-centre`}>
                    <div className={`col d-half`}>
                        <div className={`content`}>
                            <img src={logo} className="rs-logo" alt="logo" />
                        </div>
                    </div>
                </div>
            </div>
            <div className={`nav dark-bg all-light-text `}>
                <div className={`container full-width`}>
                    <div className={`row h-centre`}>
                        <Tab title={"Visualisations"} slug={''}/>
                        <Tab title={"Saved Views"} slug={'about'} />
                        <Tab title={"Apprentices"} slug={'about'} />
                    </div>
                </div>
            </div>
        </header>
    )
};
export default Header;
