import React from 'react';
import {BarChart, Bar, XAxis, YAxis, Tooltip, Legend, ReferenceLine, Label} from 'recharts';

let Rechart = (props) => {

        const Bars = () => {
            const barsToRender = props.keys.map((key, index)=> (
                <Bar dataKey={key} fill={props.colourScheme[index]} />
            ))
            return(barsToRender)
        }
        return (
            <BarChart
                width={1400}
                height={600}
                data={props.data}
                margin={{
                    top: 5,
                    right: 30,
                    left: 20,
                    bottom: 5
                }}
                barCategoryGap={36}
                barGap={12}
            >
                <XAxis dataKey="name" />
                <YAxis type="number" domain={[0, 100]}/>
                <Tooltip />
                <Legend />
                {Bars()}
                <ReferenceLine y="70" stroke="#cfcfcf" > <Label value="First Class (1st)" position="insideRight"/> </ReferenceLine>
                <ReferenceLine y="60" stroke="#cfcfcf" > <Label value="Upper Second Class (2:1)" position="insideRight"/> </ReferenceLine>
                <ReferenceLine y="50" stroke="#cfcfcf" > <Label value="Lower Second Class (2:2)" position="insideRight"/> </ReferenceLine>
                <ReferenceLine y="40" stroke="#cfcfcf" > <Label value="Third Class (3rd)" position="insideRight"/> </ReferenceLine>
            </BarChart>
        );
}

export default Rechart;
