import * as React from "react";
import Button from "./Button";


const VisTabsRow = (props) => {
    return (
        <div className={`row h-centre `}>
            <div className={`col d-half`}>
                <Button name={props.name1} description={props.description1} />
            </div>
            <div className={`col d-half`}>
                <Button name={props.name2} description={props.description2} />
            </div>
        </div>
    )
};

const VisTabs = () => {
    return (
        <div className={`viz-tabs`}>
            <VisTabsRow name1={"Module Results"} description1={"Overview of student results filterable by module"} name2={"Student results"} description2={"Overview of student results filterable by student"}  />
            <VisTabsRow name1={"Grades per tool"} description1={"Average grade achieved in modules tagged with different tools"} name2={"Performance per tool"} description2={"Overview of grade boundaries being achieved for each tool"}/>
        </div>
    )
};
export default VisTabs;
