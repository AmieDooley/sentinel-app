const studentGradesPerTool = [
    {
        employeeID: "1",
        name: "John",
        Javascript: 81,
        Java: 63,
        SQL: 68,
        React: 71,
        Python: 74
    },
    {
        employeeID: "2",
        name: "Ben",
        Javascript: 83,
        Java: 59,
        SQL: 66,
        React: 82,
        Python: 81
    },
    {
        employeeID: "1",
        name: "Callum",
        Javascript: 74,
        Java: 58,
        SQL: 74,
        React: 45,
        Python: 68
    },
    {
        employeeID: "2",
        name: "Louise",
        Javascript: 74,
        Java: 69,
        SQL: 69,
        React: 72,
        Python: 69
    },
    {
        employeeID: "1",
        name: "Beth",
        Javascript: 72,
        Java: 57,
        SQL: 64,
        React: 74,
        Python: 71
    },
    {
        employeeID: "2",
        name: "Sophie",
        Javascript: 84,
        Java: 54,
        SQL: 72,
        React: 69,
        Python: 73
    }
];

export default studentGradesPerTool;
