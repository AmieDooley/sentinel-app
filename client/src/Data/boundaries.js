const boundaries = [
    {valueUpper : 100, valueLower: 70, label: 'First Class (1st)'},
    {valueUpper : 69, valueLower: 60, label: 'Upper Second Class (2:1)'},
    {valueUpper : 59, valueLower: 50, label: 'Lower Second Class (2:2)'},
    {valueUpper : 49, valueLower: 40, label: 'Third Class (3rd)'},
    {valueUpper : 39, valueLower: 0, label: 'Fail'},
];

export default boundaries;

