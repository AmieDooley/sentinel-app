const studentGradesPerModule = [
    {
        employeeID: "1",
        name: "John",
        "Dissertation": 89,
        "Security": 63,
        "Proposal": 85,
        "Quality Mgm.": 89,
        "App Dev": 74
    },
    {
        employeeID: "5",
        name: "Ben",
        "Dissertation": 79,
        "Security": 57,
        "Proposal": 94,
        "Quality Mgm.": 74,
        "App Dev": 71

    },
    {
        employeeID: "3",
        name: "Callum",
        "Dissertation": 74,
        "Security": 64,
        "Proposal": 84,
        "Quality Mgm.": 45,
        "App Dev": 68
    },
    {
        employeeID: "2",
        name: "Louise",
        "Dissertation": 88,
        "Security": 59,
        "Proposal": 91,
        "Quality Mgm.": 82,
        "App Dev": 81
    },
    {
        employeeID: "4",
        name: "Beth",
        "Dissertation": 79,
        "Security": 71,
        "Proposal": 79,
        "Quality Mgm.": 72,
        "App Dev": 69
    },
    {
        employeeID: "6",
        name: "Sophie",
        "Dissertation": 89,
        "Security": 54,
        "Proposal": 92,
        "Quality Mgm.": 69,
        "App Dev": 73
    }
];

export default studentGradesPerModule;