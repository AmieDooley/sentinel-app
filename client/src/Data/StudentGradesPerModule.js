const studentGradesPerModule = [
    {
        employeeID: "1",
        name: "John",
        Programming: 89,
        Algorithms: 63,
        Databases: 89,
        "Adv.Programming": 71,
        "App Development": 74
    },
    {
        employeeID: "2",
        name: "Ben",
        Programming: 83,
        Algorithms: 59,
        Databases: 91,
        "Adv.Programming": 82,
        "App Development": 81
    },
    {
        employeeID: "1",
        name: "Callum",
        Programming: 74,
        Algorithms: 64,
        Databases: 84,
        "Adv.Programming": 45,
        "App Development": 68
    },
    {
        employeeID: "2",
        name: "Louise",
        Programming: 79,
        Algorithms: 71,
        Databases: 79,
        "Adv.Programming": 72,
        "App Development": 69
    },
    {
        employeeID: "1",
        name: "Beth",
        Programming: 79,
        Algorithms: 57,
        Databases: 94,
        "Adv.Programming": 74,
        "App Development": 71
    },
    {
        employeeID: "2",
        name: "Sophie",
        Programming: 89,
        Algorithms: 54,
        Databases: 92,
        "Adv.Programming": 69,
        "App Development": 73
    }
];

export default studentGradesPerModule;