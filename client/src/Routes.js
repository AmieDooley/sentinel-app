import React, { Component } from "react";
import { Router, Switch, Route } from "react-router-dom";

import ModuleResults from "./Pages/ModuleResults";
import StudentResults from "./Pages/StudentResults";
import About from "./Pages/About";
import Home from "./Pages/Home";
import ToolResults from "./Pages/ToolResults";
import PerformancePerTool from "./Pages/PerformancePerTool";
import history from './history';

export default class Routes extends Component {
    render() {
        return (
            <Router history={history}>
                <Switch>
                    <Route path="/" exact component={Home} />
                    <Route path="/viz/module-results" component={ModuleResults} />
                    <Route path="/viz/student-results" component={StudentResults} />
                    <Route path="/viz/grades-per-tool" component={ToolResults} />
                    <Route path="/viz/performance-per-tool" component={PerformancePerTool} />
                </Switch>
            </Router>
        )
    }
}