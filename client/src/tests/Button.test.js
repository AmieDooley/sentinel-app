import Button from "../Components/Button";
import { mount, render } from 'enzyme';
import Home from "../Pages/Home";
import React from "react";

test('Button text is correct', () => {
    let buttonName = 'Test button';
    let buttonDesc = 'Test description'
    let button = render(<Button name={buttonName} description={buttonDesc} />);

    expect(button.find('.lg-button').text().includes(buttonName)).toBe(true);
    expect(button.find('p').text().includes(buttonDesc)).toBe(true);
})



test('button slug is set correctly', () => {
    let button = mount(<Button name={'Test button'} description={'description'} />)
    expect(button.find('a').prop('href')).toEqual('/viz/test-button')
    button.unmount();
})
