import { render, screen } from '@testing-library/react';
import App from '../App';


describe('should render everything on category page', () => {
  test('Page title', () => {
    render(<App />);
    const linkElement = screen.getByText('Categories');
    expect(linkElement).toBeInTheDocument();
  });
})

describe('Category buttons link to correct pages', () => {
  test('Module results link', () => {
    render(<App />);
    const button = screen.getByText('Module Results');
    expect(button.closest('a')).toHaveAttribute('href', '/viz/module-results')
  })

  test('Student results link', () => {
    render(<App />);
    const button = screen.getByText('Student results');
    expect(button.closest('a')).toHaveAttribute('href', '/viz/student-results')
  })

  test('Grades per tool link', () => {
    render(<App />);
    const button = screen.getByText('Grades per tool');
    expect(button.closest('a')).toHaveAttribute('href', '/viz/grades-per-tool')
  })

  test('Performance per tool link', () => {
    render(<App />);
    const button = screen.getByText('Performance per tool');
    expect(button.closest('a')).toHaveAttribute('href', '/viz/performance-per-tool')
  })
})
