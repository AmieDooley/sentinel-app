import { mount, render } from 'enzyme';
import Filter from "../Components/Filter";
import React from "react";
import colourBlind from "../Colours/ColourBlind";
import reds from "../Colours/Reds";
import blues from "../Colours/Blues";
import {fireEvent} from "@testing-library/react";

test('Filter title is set correctly', () => {
    let title = 'Test filter';
    let filter = render(<Filter title={title}/>);

    expect(filter.find('p').text().includes(title)).toBe(true);
})


