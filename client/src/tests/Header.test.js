// import { render, screen } from '@testing-library/react';
import Header from "../Components/Header";
import { mount, render } from 'enzyme';


test('Renders the RS logo', () => {
    let header = render(<Header />);
    expect(header.find('img').prop('src')).toEqual('rsLogo.png');
});

test('renders three tabs', () => {
    let header = render(<Header />);
    expect(header.find('.menu-tab').length).toEqual(3);
});

test('tabs have correct names', () => {
    let header = mount(<Header />);
    expect(header.find('.menu-tab').at(0).text().includes('Visualisations')).toBe(true);
    expect(header.find('.menu-tab').at(1).text().includes('Saved Views')).toBe(true);
    expect(header.find('.menu-tab').at(2).text().includes('Apprentices')).toBe(true);
    header.unmount();
});

test('has correct style classes', () => {
    let header = mount(<Header />);
    expect(header.find('.nav').hasClass('dark-bg')).toBe(true);
    expect(header.find('.nav').hasClass('all-light-text')).toBe(true);
    header.unmount();
})
