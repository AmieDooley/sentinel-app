import "./App.css";
import * as React from "react";
import Header from "./Components/Header";
import { BrowserRouter as Router } from 'react-router-dom';
import Routes from './Routes';

const App = () => {
  return (
    <div className="App">
      <Router>
        <div className="App">
          <Header/>
          <Routes />
        </div>
      </Router>
      </div>
  );
};
export default App;
