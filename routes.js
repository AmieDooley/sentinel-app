const Router = require('express')
const db = require("./db/index")
const admin = require('./db/main');
const router = Router();

router.get('/api/users', async (req, res) => {
  let data;
    try{
      data = await admin.getAllStudents()
    } catch (err){
      data = {"error": err}
    }
  res.send(data);
})

router.get('/api/studentsModules', async (req, res) => {
  let data;
  try{
    data = await admin.getAllStudentModuleGrades()
  } catch (err){
    data = {"error": err}
  }
  res.send(data);
})


module.exports = router;
