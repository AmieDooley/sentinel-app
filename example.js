let json = {
    E0123: {
        name: "amie dooley",
        modules: [123, 456,789]
    },
    E0455: {
        name: "mark Smith",
        modules: [353, 415, 623]
    }

}

let arr = [
    {
        employeeID: "E0123",
        name: "amie dooley",
        modules: [123, 456,789]
    },
    {
        employeeID: "E0455",
        name: "mark smith",
        modules: [353, 415, 623]
    }
]